#!/usr/bin/env python3
import requests
import json
import sys
from typing import List, Optional, Set
from dataclasses import dataclass
from datetime import datetime

@dataclass
class CoreOSRelease:
    version: str
    kernel_version: str
    release_date: datetime
    architecture: str
    release_type: str

def debug_print(*args, **kwargs):
    """Helper function to print debug messages to stderr"""
    print(*args, file=sys.stderr, **kwargs)

def get_latest_coreos_kernel_versions(
    n: int = 3,
    arch: str = "x86_64",
    release_types: Set[str] = {"metal"},
    debug: bool = True
) -> List[str]:
    """
    Fetch the latest n kernel versions from Fedora CoreOS releases.

    Args:
        n: Number of latest unique kernel versions to retrieve
        arch: Architecture to filter by (e.g., 'x86_64', 'aarch64')
        release_types: Set of release types to include (e.g., {'metal', 'qemu'})
        debug: Enable debug output

    Returns:
        List of kernel version strings sorted by release date (newest first)
    """
    try:
        if debug:
            debug_print(f"Fetching Fedora CoreOS release data...")
            debug_print(f"Filtering for arch: {arch}")
            debug_print(f"Release types: {release_types}")

        stream_url = "https://builds.coreos.fedoraproject.org/streams/stable.json"
        response = requests.get(stream_url, timeout=10)
        response.raise_for_status()
        stream_data = response.json()

        fedora_version = None
        for artifact in stream_data.get("architectures", {}).get(arch, {}).get("artifacts", {}).values():
            if "release" in artifact:
                fedora_version = artifact["release"].split(".")[0]
                break

        if not fedora_version:
            debug_print("Could not determine Fedora version", file=sys.stderr)
            return []

        if debug:
            debug_print(f"Found Fedora version: {fedora_version}")

        bodhi_url = "https://bodhi.fedoraproject.org/updates/"
        params = {
            'packages': 'kernel',
            'release': f'F{fedora_version}',
            'status': 'stable',
            'rows_per_page': n
        }

        if debug:
            debug_print(f"Querying Bodhi API for kernel updates...")

        response = requests.get(bodhi_url, params=params, timeout=10)
        response.raise_for_status()
        updates_data = response.json()

        kernel_versions = []
        seen_versions = set()

        for update in updates_data.get('updates', []):
            for build in update.get('builds', []):
                nvr = build.get('nvr', '')
                if nvr.startswith('kernel-'):
                    version = nvr.split('kernel-')[1]
                    if version not in seen_versions:
                        seen_versions.add(version)
                        kernel_versions.append(version)
                        if debug:
                            debug_print(f"Found kernel version: {version}")

                        if len(kernel_versions) >= n:
                            break

            if len(kernel_versions) >= n:
                break

        if debug:
            debug_print(f"\nFound {len(kernel_versions)} kernel versions")

        return kernel_versions

    except requests.exceptions.RequestException as e:
        debug_print(f"Error fetching data: {e}", file=sys.stderr)
        return []
    except Exception as e:
        debug_print(f"Unexpected error: {e}", file=sys.stderr)
        debug_print(f"Error details: {str(e)}", file=sys.stderr)
        return []

if __name__ == "__main__":
    debug_print("Fetching latest Fedora CoreOS kernel versions...")
    latest_kernels = get_latest_coreos_kernel_versions(
        n=3,
        arch="x86_64",
        release_types={"metal"},
        debug=True
    )

    if latest_kernels:
        # Print kernel versions to stdout for easy parsing
        for version in latest_kernels:
            print(version)
    else:
        debug_print("\nNo kernel versions found!", file=sys.stderr)
