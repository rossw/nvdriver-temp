import requests
from bs4 import BeautifulSoup

def version_to_tuple(version):
    # Split version into parts
    parts = version.split('.')

    # Convert each part to integers, handling special cases
    result = []
    for part in parts:
        try:
            # Handle parts like '86' in '570.86.16'
            result.append(int(part))
        except ValueError:
            # Handle parts like '0-4499' by taking the last number
            subparts = part.split('-')
            try:
                result.append(int(subparts[-1]))
            except (ValueError, IndexError):
                # If conversion fails, use 0 as fallback
                result.append(0)
    return tuple(result)

def get_nvidia_versions():
    url = "https://download.nvidia.com/XFree86/Linux-x86_64/"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')

    versions = []
    for link in soup.find_all('a'):
        version = link.get('href').strip('/')
        # Only consider version directories that start with a number
        if version and version[0].isdigit():
            versions.append(version)

    # Sort versions using our custom version comparison
    versions.sort(key=version_to_tuple)

    # Return the latest 3 versions
    return versions[-3:]

if __name__ == "__main__":
    for version in get_nvidia_versions():
        print(version)
