# Build stage
FROM fedora:${FEDORA_VERSION} as builder

ARG KERNEL_VERSION
ARG DRIVER_VERSION
ARG FEDORA_VERSION

# Install build dependencies
RUN dnf install -y \
    gcc \
    make \
    elfutils-libelf-devel \
    rpm-build \
    kmod \
    file \
    curl \
    ca-certificates \
    pkg-config \
    kernel-headers \
    xorg-x11-proto-devel \
    xorg-x11-server-devel \
    createrepo_c

# Download kernel packages and create local repo
RUN echo "Installing kernel version: ${KERNEL_VERSION}" && \
    KERNEL_VERSION_X86_64="${KERNEL_VERSION}.x86_64" && \
    rm -rf /tmp/kernel-rpms && \
    mkdir -p /tmp/kernel-rpms && \
    cd /tmp/kernel-rpms && \
    # Download all required RPMs
    curl -L -O \
    "https://kojipkgs.fedoraproject.org/packages/kernel/${KERNEL_VERSION%%-*}/${KERNEL_VERSION#*-}/x86_64/kernel-modules-core-${KERNEL_VERSION_X86_64}.rpm" && \
    curl -L -O \
    "https://kojipkgs.fedoraproject.org/packages/kernel/${KERNEL_VERSION%%-*}/${KERNEL_VERSION#*-}/x86_64/kernel-core-${KERNEL_VERSION_X86_64}.rpm" && \
    curl -L -O \
    "https://kojipkgs.fedoraproject.org/packages/kernel/${KERNEL_VERSION%%-*}/${KERNEL_VERSION#*-}/x86_64/kernel-modules-${KERNEL_VERSION_X86_64}.rpm" && \
    curl -L -O \
    "https://kojipkgs.fedoraproject.org/packages/kernel/${KERNEL_VERSION%%-*}/${KERNEL_VERSION#*-}/x86_64/kernel-${KERNEL_VERSION_X86_64}.rpm" && \
    curl -L -O \
    "https://kojipkgs.fedoraproject.org/packages/kernel/${KERNEL_VERSION%%-*}/${KERNEL_VERSION#*-}/x86_64/kernel-devel-${KERNEL_VERSION_X86_64}.rpm" && \
    curl -L -O \
    "https://kojipkgs.fedoraproject.org/packages/kernel/${KERNEL_VERSION%%-*}/${KERNEL_VERSION#*-}/x86_64/kernel-devel-matched-${KERNEL_VERSION_X86_64}.rpm" && \
    # Create repository
    createrepo_c . && \
    # Configure DNF to use local repository
    echo -e "[kernel-local]\nname=Kernel Local Repository\nbaseurl=file:///tmp/kernel-rpms\nenabled=1\ngpgcheck=0" > /etc/yum.repos.d/kernel-local.repo && \
    # Install packages from local repo
    dnf --setopt=keepcache=1 install -y \
    kernel-modules-core-${KERNEL_VERSION_X86_64} \
    kernel-core-${KERNEL_VERSION_X86_64} \
    kernel-modules-${KERNEL_VERSION_X86_64} \
    kernel-${KERNEL_VERSION_X86_64} \
    kernel-devel-${KERNEL_VERSION_X86_64} \
    kernel-devel-matched-${KERNEL_VERSION_X86_64} && \
    # Verify installation
    echo "Installed kernel packages:" && \
    rpm -qa | grep kernel

# Download and install NVIDIA driver
RUN set -e && \
    rm -rf /tmp/nvidia-build && \
    mkdir -p /tmp/nvidia-build && \
    cd /tmp/nvidia-build && \
    curl -fsSL -o driver.run "https://download.nvidia.com/XFree86/Linux-x86_64/${DRIVER_VERSION}/NVIDIA-Linux-x86_64-${DRIVER_VERSION}.run" && \
    sh driver.run -x && \
    cd NVIDIA-Linux-x86_64-${DRIVER_VERSION} && \
    echo "Building NVIDIA kernel modules..." && \
    IGNORE_MISSING_MODULE_SYMVERS=1 \
    KERNEL_UNAME="${KERNEL_VERSION}.x86_64" \
    ./nvidia-installer --silent \
    --kernel-source-path="/usr/src/kernels/${KERNEL_VERSION}.x86_64" \
    --no-nouveau-check \
    --no-opengl-files \
    --no-distro-scripts \
    --no-backup \
    --no-check-for-alternate-installs \
    --no-x-check \
    --no-systemd \
    --install-compat32-libs \
    --log-file-name=/tmp/nvidia-installer.log \
    -a && \
    # Verify modules were built
    if [ ! -f "kernel/nvidia.ko" ]; then \
    echo "ERROR: NVIDIA kernel modules failed to build" && \
    cat /tmp/nvidia-installer.log && \
    exit 1; \
    fi && \
    # Create organized directory structure
    rm -rf /tmp/nvidia-files && \
    mkdir -p /tmp/nvidia-files/usr/bin && \
    mkdir -p /tmp/nvidia-files/usr/lib64 && \
    mkdir -p /tmp/nvidia-files/lib/modules/${KERNEL_VERSION}.x86_64/kernel/drivers/video/nvidia && \
    # Copy files
    cp -a nvidia-smi nvidia-modprobe nvidia-persistenced /tmp/nvidia-files/usr/bin/ && \
    cp -a kernel/nvidia*.ko /tmp/nvidia-files/lib/modules/${KERNEL_VERSION}.x86_64/kernel/drivers/video/nvidia/ && \
    cp -a *.so.* /tmp/nvidia-files/usr/lib64/ && \
    # Verify copied modules
    if [ -z "$(ls -A /tmp/nvidia-files/lib/modules/${KERNEL_VERSION}.x86_64/kernel/drivers/video/nvidia/)" ]; then \
    echo "ERROR: Failed to copy NVIDIA kernel modules" && \
    exit 1; \
    fi

# Final stage
FROM fedora:${FEDORA_VERSION}

ARG KERNEL_VERSION
ARG DRIVER_VERSION

COPY --from=builder /tmp/nvidia-files/usr/bin/* /usr/bin/
COPY --from=builder /tmp/nvidia-files/usr/lib64/* /usr/lib64/
COPY --from=builder /tmp/nvidia-files/lib/modules/ /lib/modules/
COPY nvidia-driver /usr/local/bin/

RUN dnf install -y --setopt=install_weak_deps=False \
    kmod \
    pciutils \
    procps-ng \
    util-linux \
    && dnf clean all && \
    rm -rf /var/cache/dnf/* && \
    rm -rf /tmp/* /var/tmp/*

# Setup symlinks and permissions
RUN chmod +x /usr/local/bin/nvidia-driver && \
    depmod ${KERNEL_VERSION}.x86_64 && \
    mkdir -p /run/nvidia/driver/lib/firmware && \
    if [ ! -f "/lib/modules/${KERNEL_VERSION}.x86_64/kernel/drivers/video/nvidia/nvidia.ko" ]; then \
    echo "ERROR: NVIDIA kernel modules not found in final location" && \
    exit 1; \
    fi

WORKDIR /drivers

ENV DRIVER_VERSION=${DRIVER_VERSION} \
    DRIVER_BRANCH=${DRIVER_VERSION}

ENTRYPOINT ["nvidia-driver", "init"]
